|||||||||||||||||||||||||||||||
|_-=[Python Auto Wordpress]=-_|
|||||||||||||||||||||||||||||||

Introducción:

Script para la instalación de wordpress en plataformas Debian, el script descarga la última versión de wordpress, crea la base de datos 
en MySQL y añade los parámetros necesarios al fichero de configuración, los datos necesarios como la clave de la base de datos, 
el nombre del wordpress, etc, los pide de manera interactiva. 

Requerimientos:

-Permisos de root.
-Instalación de Apache2 y el módulo de PHP.
-Tener creada una base de datos de prueba a la que conectarse
en el script está escrita la bd "prueba1" por defecto.
-Sistema Debian Wheezy o Debian Jessie.